# Foursquare - Location Matching

Match point of interest data across datasets

## Sobre

Quando você procura restaurantes próximos ou planeja um recado em uma área desconhecida, espera informações relevantes e precisas. Manter dados de qualidade em todo o mundo é um desafio e com implicações além da navegação. As empresas tomam decisões sobre novos sites para expansão de mercado, analisam o cenário competitivo e exibem anúncios relevantes informados por dados de localização. Para esses e muitos outros usos, dados confiáveis ​​são essenciais.

Conjuntos de dados em grande escala sobre pontos de interesse comerciais (POI) podem ser ricos em informações do mundo real. Para manter o mais alto nível de precisão, os dados devem ser correspondidos e desduplicados com atualizações oportunas de várias fontes. A desduplicação envolve muitos desafios, pois os dados brutos podem conter ruído, informações não estruturadas e atributos incompletos ou imprecisos. Uma combinação de algoritmos de aprendizado de máquina e métodos rigorosos de validação humana são ideais para desduplicar conjuntos de dados.

Com mais de 12 anos de experiência aperfeiçoando esses métodos, o Foursquare é o fornecedor independente nº 1 de dados globais de POI. A líder em tecnologia de localização independente e plataforma de nuvem de dados, o Foursquare é dedicado a construir pontes significativas entre espaços digitais e locais físicos. Com a confiança de empresas líderes como Apple, Microsoft, Samsung e Uber, a pilha de tecnologia do Foursquare aproveita o poder dos lugares e do movimento para melhorar as experiências dos clientes e gerar melhores resultados de negócios.

Nesta competição, você combinará os POIs. Usando um conjunto de dados de mais de um milhão e meio de entradas de Places fortemente alteradas para incluir ruído, duplicações, informações irrelevantes ou incorretas, você produzirá um algoritmo que prevê quais entradas de Places representam o mesmo ponto de interesse. Cada entrada Place inclui atributos como nome, endereço e coordenadas. Os envios bem-sucedidos identificarão as correspondências com a maior precisão.

Ao combinar POIs de maneira eficiente e bem-sucedida, você facilitará a identificação de onde novas lojas ou negócios beneficiariam mais as pessoas.

## Pontuação

As inscrições são avaliadas pela média da [Interseção sobre a União](https://en.wikipedia.org/wiki/Jaccard_index) (IoU, também conhecido como índice Jaccard) das correspondências de entrada de verdade e das correspondências de entrada previstas. A média é obtida por amostragem, o que significa que uma pontuação de IoU é calculada para cada linha no arquivo de envio e a pontuação final é a média.

Arquivo de Submissão

Para cada ID de entrada de local no conjunto de teste, você deve enviar uma lista delimitada por espaços de IDs de local correspondentes. Os lugares sempre correspondem a si mesmos, portanto, a lista de correspondências para um id deve sempre conter esse id.

O arquivo deve conter um cabeçalho, ser nomeado submit.csv e ter o seguinte formato:

id,matches
E_00001118ad0191,E_00001118ad0191
E_000020eb6fed40,E_000020eb6fed40
E_00002f98667edf,E_00002f98667edf
E_001b6bad66eb98,E_001b6bad66eb98 E_0283d9f61e569d
E_0283d9f61e569d,E_0283d9f61e569d E_001b6bad66eb98

Você deve prever correspondências para cada id. Por exemplo, se você acredita que A corresponde a B e C, seu arquivo de envio deve incluir as linhas A,A B C, mas também B,B A C e C,C A B.

## Timeline


- April 14, 2022 - Start Date.
- June 30, 2022 - Entry Deadline. You must accept the competition rules before this date in order to compete.
- June 30, 2022 - Team Merger Deadline. This is the last day participants may join or merge teams.
- July 7, 2022 - Final Submission Deadline.

Todos os prazos são às 23h59 UTC do dia correspondente, salvo indicação em contrário. Os organizadores do concurso reservam-se o direito de atualizar o cronograma do concurso se julgarem necessário.

## Requisitos do código

Esta é uma competição de código

As inscrições para este concurso devem ser feitas através de Notebooks. Para que o botão "Enviar" fique ativo após um commit, as seguintes condições devem ser atendidas:

- CPU Notebook <= 9 horas de tempo de execução
- Notebook GPU <= 9 horas de tempo de execução
- Acesso à Internet desativado
- Dados externos disponíveis gratuitamente e publicamente são permitidos, incluindo modelos pré-treinados
- O arquivo de envio deve ser nomeado submit.csv

Consulte as Perguntas frequentes sobre a competição de códigos para obter mais informações sobre como enviar. E revise o documento de depuração de código se estiver encontrando erros de envio.

## Descrição dos Dados

Os dados apresentados aqui compreendem mais de um milhão e meio de entradas de lugares para centenas de milhares de Pontos de Interesse (POIs) comerciais em todo o mundo. Sua tarefa é determinar quais entradas de local descrevem o mesmo ponto de interesse. Embora as entradas de dados possam representar ou assemelhar-se a entradas de lugares reais, elas também podem conter informações artificiais ou ruídos adicionais.

## Dados de treinamento

**train.csv** - O conjunto de treinamento, composto por onze campos de atributos para mais de um milhão de entradas de lugares, juntamente com:
- **id** - Um identificador exclusivo para cada entrada.
- **point_of_interest** - Um identificador para o POI que a entrada representa. Pode haver uma ou várias entradas descrevendo o mesmo POI. Duas entradas "correspondem" quando descrevem um POI comum.
**pairs.csv** - Um conjunto pré-gerado de pares de entradas de lugar de train.csv projetado para melhorar a detecção de correspondências. Você pode querer gerar pares adicionais para melhorar a capacidade do seu modelo de discriminar POIs.
- match - Se (True ou False) o par de entradas descreve um POI comum.

## Dados de teste de exemplo

Para ajudá-lo a criar o código de envio, incluímos algumas instâncias de exemplo selecionadas do conjunto de teste. Quando você enviar seu notebook para pontuação, esses dados de exemplo serão substituídos pelos dados de teste reais. O conjunto de teste real tem aproximadamente 600.000 entradas de local com POIs que são distintos dos POIs no conjunto de treinamento.

**test.csv** - Um conjunto de entradas de local com seus campos de atributos registrados, semelhantes ao conjunto de treinamento.
**sample_submission.csv** - Um arquivo de envio de amostra no formato correto.
- id - O identificador exclusivo para uma entrada de local, um para cada entrada no conjunto de teste.
- matches - Uma lista de IDs delimitada por espaço para entradas no conjunto de teste que correspondem ao ID fornecido. As entradas de lugar sempre correspondem a si mesmas.
